import csv
import matplotlib.pyplot as plt

def plot_population_data(data_csv_path, region_name):
    """
    Plot population data for a specific region from a CSV file.

    Args:
    data_csv_path (str): Path to the CSV data file.
    region_name (str): Name of the region to plot.

    Returns:
    None
    """
    # Create an empty dictionary to store population data
    population = {}

    # Open the CSV file
    with open(data_csv_path, newline='') as csvfile:
        data = csv.DictReader(csvfile)

        # Iterate through the CSV data
        for row in data:
            # Check if the region matches the specified region_name
            if row['Region'] == region_name:
                # Store the population for each year in the dictionary
                population[row['Year']] = float(row['Population'])

    # Plot the data
    plt.plot(population.keys(), population.values())
    plt.xticks(rotation=90)  # Rotate the x-axis labels for better readability
    plt.ylabel('Population in Billions')
    plt.xlabel('Years')
    plt.title(f'Population of {region_name} over the Years')
    plt.show()

# Usage
data_csv_path = 'Dataset/population-estimates_csv.csv'
region_name = 'India'
plot_population_data(data_csv_path, region_name)
