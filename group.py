import csv
import matplotlib.pyplot as plt

def transform_data(data_csv_path, countries):
    """
    Transform data from a CSV file into a format suitable for plotting.

    Args:
    data_csv_path (str): Path to the CSV data file.
    countries (list): List of ASEAN countries to consider.

    Returns:
    years (list): List of years (2004-2014).
    populations (dict): A dictionary containing populations for each country over the years.
    """
    years = [str(year) for year in range(2004, 2015)]
    ASEAN_POPULATION = {}

    with open(data_csv_path, newline='') as csvfile:
        data = csv.DictReader(csvfile)

        for row in data:
            if row['Region'] in countries and row['Region'] not in ASEAN_POPULATION and 2004 <= int(row['Year']) <= 2014:
                ASEAN_POPULATION[row['Region']] = {row["Year"]: float(row['Population'])}
            elif row['Region'] in countries and row['Region'] in ASEAN_POPULATION:
                ASEAN_POPULATION[row['Region']].update({row["Year"]: float(row['Population'])})

    populations = {nation: [ASEAN_POPULATION[nation][year] for year in years] for nation in ASEAN_POPULATION}

    return years, populations

def plot_grouped_bar_chart(years, populations, countries):
    """
    Create a grouped bar chart from transformed data.

    Args:
    years (list): List of years (2004-2014).
    populations (dict): A dictionary containing populations for each country over the years.
    countries (list): List of ASEAN countries to consider.

    Returns:
    None
    """
    # Define the positions and width for the bars
    x = list(range(len(years)))
    width = 0.1

    # Create a figure and axis for the plot
    fig, ax = plt.subplots(figsize=(15, 8))

    # Loop through the nations and plot their population data as grouped bars
    for i, nation in enumerate(countries):
        plt.bar([pos + i * width for pos in x], populations[nation], width=width, label=nation)

    # Set the labels, title, and legend
    plt.xlabel('Year')
    plt.ylabel('Population')
    plt.title('Population of ASEAN Nations (2004-2014)')
    plt.xticks([pos + width * 4.5 for pos in x], years)
    plt.legend(title='Nations', loc='upper left', bbox_to_anchor=(1, 1))

    # Show the plot
    plt.tight_layout()
    plt.show()


data_csv_path = 'Dataset/population-estimates_csv.csv'
countries = ["Brunei Darussalam", "Cambodia", "Indonesia", "Lao People's Democratic Republic", "Malaysia", "Myanmar", "Philippines", "Singapore", "Thailand", "Viet Nam"]
    
years, populations = transform_data(data_csv_path, countries)
plot_grouped_bar_chart(years, populations, countries)