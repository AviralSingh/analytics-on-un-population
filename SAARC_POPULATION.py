import csv
import matplotlib.pyplot as plt

# List of SAARC countries
# List of SAARC countries
saarc_countries = ["Afghanistan", "Bangladesh", "Bhutan", "India", "Maldives", "Nepal", "Pakistan", "Sri Lanka"]

# Function to transform the data
def transform_data(csv_file_path, countries):
    saarc_population = {}
    
    with open(csv_file_path, newline='') as csvfile:
        data = csv.DictReader(csvfile)

        for row in data:
            if row['Region'] in countries:
                year = row['Year']
                population = float(row['Population'])
                if year not in saarc_population:
                    saarc_population[year] = population
                else:
                    saarc_population[year] += population

    return saarc_population

# Function to create and display the plot
def create_and_show_plot(data_dict):
    plt.bar(data_dict.keys(), data_dict.values())
    plt.xticks(rotation=90)  # Rotate the x-axis labels for better readability
    plt.show()

# Usage
csv_file_path = 'Dataset/population-estimates_csv.csv'
saarc_population_data = transform_data(csv_file_path, saarc_countries)
create_and_show_plot(saarc_population_data)
