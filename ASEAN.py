# Import necessary libraries
import csv
import matplotlib.pyplot as plt

# Function to process data and create the pie chart
def process_and_plot_asean_population(data_csv_path, year='2014'):
    # List of ASEAN countries
    Countries = ["Brunei Darussalam", "Cambodia", "Indonesia", "Lao People's Democratic Republic", "Malaysia", "Myanmar", "Philippines", "Singapore", "Thailand", "Viet Nam"]

    # Create an empty dictionary to store population data
    ASEAN_POPULATION = {}

    # Open the CSV file
    with open(data_csv_path, newline='') as csvfile:
        data = csv.DictReader(csvfile)

        # Iterate through the CSV data
        for row in data:
            # Check if the country is in the ASEAN list and the year matches the specified year
            if row['Region'] in Countries and row['Year'] == year:
                # Store the population data in the dictionary
                ASEAN_POPULATION[row["Region"]] = float(row['Population'])

    # Return the populated dictionary
    return ASEAN_POPULATION

# Function to create and display the pie chart
def plot_asean_population(ASEAN_POPULATION):
    # Create a pie chart using the population data
    plt.pie(ASEAN_POPULATION.values(), labels=ASEAN_POPULATION.keys())
    # Display the pie chart
    plt.show()

# Usage
data_csv_path = 'Dataset/population-estimates_csv.csv'
year = '2014'
ASEAN_POPULATION = process_and_plot_asean_population(data_csv_path, year)
# Call the function to plot the pie chart
plot_asean_population(ASEAN_POPULATION)
